import React from 'react';
import {useState} from "react";
import {useEffect} from "react";
import './CountriesMenu.css'
import axios from "axios";

const url = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';

const CountriesMenu = () => {
    const [countries, setCountries] = useState([]);
    const [country, setCountry] = useState(null);
    const [borders, setBorders] = useState([]);


    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url);

            if (response.ok) {
                const countries = await response.json();
                setCountries(countries);
            }
        };
        fetchData().catch(e => console.error(e));
    }, []);

    const chooseCountry = (alpha) => {
        const fetchData = async () => {
            const response = await fetch(`https://restcountries.eu/rest/v2/alpha/${alpha}`);

            if (response.ok) {
                const country = await response.json();
                setCountry(country);

                const alpha3 = [];
                country.borders.forEach(post => {
                    alpha3.push(post);
                });

                const responses = await Promise.all(
                    alpha3.map(id => axios.get('https://restcountries.eu/rest/v2/alpha/' + id)));

                setBorders(responses.map(e => {
                    return (<p>{e.data.name}</p>);
                }))
            }
        };
        fetchData().catch(e => console.error(e));
    };

    return (
        <>
            <div className='countriesMenu'>
                {countries.map(e => {
                    return (<p onClick={e => {
                        chooseCountry(e.target.className)
                    }}
                               className={e.alpha3Code}>{e.name}</p>);
                })}
            </div>
            <div>
                {country ? (<div>
                    <h3>{country.name}</h3>
                    <p>Capital: {country.capital}</p>
                    <p>Population: {country.population}</p>
                    <img style={{width: '200px', height: '200px'}} src={country.flag}/>
                    <div>
                        <h3>Borders with:</h3>
                        {borders}
                    </div>
                </div>) : null
                }
            </div>
        </>
    );
};

export default CountriesMenu;