import './App.css';
import CountriesMenu from "./components/countriesMenu/CountriesMenu";

function App() {
    return (
        <div className="App">
            <CountriesMenu/>
        </div>
    );
}

export default App;
